<?php
function print_excel_selectable_format($filename)
{
	include 'excel/PHPExcel/IOFactory.php';
	include 'excel/PHPExcel.php';
	//set_include_path(get_include_path() . PATH_SEPARATOR . '../../../excel/');
	//set_include_path("/Applications/XAMPP/xamppfiles/htdocs/pricol/excel/");
	$inputFileName = $filename;
	$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
	$objReader = PHPExcel_IOFactory::createReader($inputFileType);
	$objReader->setLoadAllSheets();
	$objPHPExcel = $objReader->load($inputFileName);
	if(isset($_REQUEST['worksheet']))
	{
		echo $html=read_sheet($_REQUEST['worksheet']);
		exit;
	}
	//echo $objPHPExcel->getSheetCount(),' worksheet',(($objPHPExcel->getSheetCount() == 1) ? '' : 's'),' loaded<br /><br />';

	$loadedSheetNames = $objPHPExcel->getSheetNames();
	$html='<select name="worksheet" id="worksheet" onchange="onworksheetchange()">';	
	foreach($loadedSheetNames as $sheetIndex => $loadedSheetName) 
	{
		$html.='<option value="'.$sheetIndex.'">'.$loadedSheetName.'</option>';
	}
	$html.='</select>';
	//read first sheet and show column details
	$html.=read_sheet(0);
}
function read_sheet($sheetindex)
{
			global $objPHPExcel;
			$worksheet = $objPHPExcel->setActiveSheetIndex($sheetindex);
			$worksheetTitle = $worksheet->getTitle();
		    $highestColumn = $worksheet->getHighestColumn();
		    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

		    // expects same number of row records for all columns
		    $highestRow = $worksheet->getHighestRow();
		    $merged_cells=$worksheet->getMergeCells();
		    $html='';
		   // $html='<div id="data_table"><table border="0">';
		    for ($row = 1; $row <= $highestRow; $row++)
		    {
		    	if(empty($worksheet->getRowDimension($row)->getVisible()))
		    		continue;
		            
		        $html.='<tr>';

		        for($col = 0; $col < $highestColumnIndex; $col++)
		    	{
		    		
		            if(empty($worksheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($col))->getVisible()))
		            	continue;

		            $cell = $worksheet->getCellByColumnAndRow($col, $row);
		            //$val = $cell->getCalculatedValue();
		            $string_coordinate=PHPExcel_Cell::stringFromColumnIndex($col).$row;;
		            $val = $cell->getFormattedValue();
		            /*if(PHPExcel_Shared_Date::isDateTime($cell)) {
					     $InvDate = date($format, PHPExcel_Shared_Date::ExcelToPHP($InvDate)); 
					}*/
		            if($cell->getStyle()->getFill()->getFillType() == PHPExcel_Style_Fill::FILL_NONE)
		            	$color='FFFFFF';
		            else
						$color=$cell->getStyle()->getFill()->getStartColor()->getRGB();
					$font_color='000000';
					$font_color=$cell->getStyle()->getFont()->getColor()->getRGB();
					
					$is_merged=false;
		            $hide_cell=false;
		            $vertical_merge=false;
		            $horizantal_merge=false;
		            $colspan=1;
		            $rowspan=1;
		            $has_image=isset($all_images[$string_coordinate]);
		            foreach($merged_cells as $mcell)
		            {	
		            	
		            	if($cell->isInRange($mcell))
		            	{
		            		$range=explode(':',$mcell);
		            		preg_match('/[^\d]+/', $range[0], $temp);
		            		$range_begins_at['col']=$temp[0];
							preg_match('/\d+/', $range[0], $temp1);
							$range_begins_at['row']=$temp1[0];

		            		preg_match('/[^\d]+/', $range[1], $temp2);
		            		$range_ends_at['col']=$temp2[0];
							preg_match('/\d+/', $range[1], $temp3);
							$range_ends_at['row']=$temp3[0];
							$current_cell=$cell->getCoordinate();
							$hide_cell=true;
							if($range[0]==$current_cell )
							{
								$hide_cell=false;
							}

							if($range_begins_at['col']==$range_ends_at['col'])
							{
								$rowspan=find_difference_in_number($range_begins_at['row'],$range_ends_at['row']);
							}
							if($range_begins_at['row']==$range_ends_at['row'])
							{

								$colspan=find_difference_in_number($range_begins_at['col'],$range_ends_at['col']);;
								
							}
							if($range_begins_at['col']!=$range_ends_at['col'] and $range_begins_at['row']!=$range_ends_at['row'])
							{
								$rowspan=find_difference_in_number($range_begins_at['row'],$range_ends_at['row']);
								$colspan=find_difference_in_number($range_begins_at['col'],$range_ends_at['col']);
							}

		            	}
	        		}
	        		if(!$hide_cell)
	        		{

		        		$html.='<td rowspan="'.$rowspan.'" colspan="'.$colspan.'" alt="'.$string_coordinate.'" class="ui-widget-content" id="'.$string_coordinate.'" data-cellid="'.$string_coordinate.'"> '.$val.' </td>';
		        	
	        		}else
	        		{
	        			//echo '<br>'.$string_coordinate;
	        		}
	        			
		        }
		        $html.='</tr>';
		    }
		    //$html.='</table></div>';
		    
		    return $html;	
}
function find_difference_in_number($a,$b)
{
	if(is_numeric($a))
	{
		return abs($a-$b)+1;
	}else
	{
		return abs(index_num_of($a)-index_num_of($b))+1;
	}
	
}
function sheet_html()
{
	$html='<!doctype html>
			<html lang="en">
			<head>
			  <meta charset="utf-8">
			  <meta name="viewport" content="width=device-width, initial-scale=1">
			  <title>jQuery UI Selectable - Default functionality</title>
			  <link rel="stylesheet" href="../boot/css/jquery-ui.css">
			 
			  <style>
			  #feedback { font-size: 1.4em; }
			  #selectable .ui-selecting { background: #FECA40; }
			  #selectable .ui-selected { background: #F39814; color: white; }
			  #selectable { list-style-type: none; margin: 0; padding: 0; width: 30%; }
			  #selectable li { margin: 3px; padding: 0.4em; font-size: 1.4em; height: 10px; }

			  #selectable tr td { margin: 3px; padding: 0.4em; font-size: 1.4em; height: 10px; }
			  </style>
			  <script src="../boot/js/jquery.min.js"></script>
			  <script src="../boot/js/jquery-ui.js"></script>
			  <script>
			    selected = new Array();
			    columnsvals= new Array();
					$(function() {
			        
					    $( "#selectable" ).selectable({
					    	filter: "td",
					        selected: function (event, ui) {
			              selected.push(ui.selected.id);
			              
			              columnsvals.push(ui.selected.innerHTML);
			            },
			            unselected: function (event,ui){
			              var f=selected.indexOf(ui.unselected.id);
			              selected.splice(f,1);
			              columnsvals.splice(f,1);
			            }
					    });


			        $( "#sub" ).click(function() {
			          $("#selected_columns").val(JSON.stringify(columnsvals)); 
			          $("#form_import").submit();
			        });

					});
			  </script>
			</head>
			<body>
			 

			 <table cellpadding="0" cellspacing="0" id="selectable">';
			 return $html;
}

function sheet_postfix($filename,$upload_id)
{
	$html='</table>

			<form action="" method="post" id="form_import">
				<input type="hidden" name="selected_columns" id="selected_columns" value="">
				<input type="hidden" name="cmd" value="columns_selected">
				<input type="hidden" name="form_name" value="'.$filename.'">
				<input type="hidden" name="upload_id" value="'.$upload_id.'">
				<input type="button" value="Next>" id="sub" name="submit_cells">
			</form>

			</body>
			</html>';

			return $html;
}
function index_num_of($g)
{
	$string = 'A';
	$i=1;
	while ($string != 'ZZ') {
	    $r[$string++]=$i++;
	}
	return $r[$g];

}

function index_of_column($g)
{
	$string = 'A';
	for($i=0;$i<=$g;$i++)
	{
		$string++;
	}
	return $string;

}

function list_all_excel($path)
{
    if(is_readable($path))
    {
        if(is_dir($path))
        {
            $list=array_diff(scandir($path), array('..', '.'));
            $html='<select name="filename" id="filename" onchange="on_file_select_change();">';
            $html.='<option value="0">Select an Excel</options>';
            foreach($list as $file)
            {
                $html.='<option value="'.$file.'">'.$file.'</options>';
            }
            $html.='</select>';
        }
    }else
    {
        $html= "change path property";
    }
    
    return $html;
}

?>