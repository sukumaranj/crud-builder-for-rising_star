<?php
include_once('../mods/config.php');
include_once('details.php');
include_once('excel.php');
include("ajax.php");
$DATA=array_merge($_POST,$_GET);
//echo '<pre>';
//print_r($DATA);
if(isset($DATA['submit_generate']) and $DATA['submit_generate']=="Generate")
{
	include("generator.php");
}else if(isset($DATA['cmd']) and $DATA['cmd']=='module_selected' and isset($DATA['id']) and $DATA['id']>0)
{
		$result=$mysqli->query("select * from builder_uploads where id='".$DATA['id']."'");
		while($line=$result->fetch_array(MYSQLI_ASSOC))
		{
			$DATA=unserialize($line['details']);
			$DATA['upload_id']=$line['id'];
			if(strlen($line['file_name'])>0)
			$DATA['form_name']=clean(trim($line['file_name']),true);
			else
			$DATA['form_name']=clean(trim($line['form_name']),true);
		}

		$form_name=$DATA['form_name'];		
		$table_name=clean($form_name,true);
		$html='
			
			<link href="../boot/css/jquery-ui.css" rel="stylesheet">
			<script src="../boot/js/jquery.min.js"></script>
			<script src="../boot/js/jquery-ui.js"></script>
			<script type="text/javascript">
			function select_changed(sel)
			{
				selected=sel.id;
				
				number=selected.replace(/\D/g,"");
				type=$(sel).val();
				$.get("import.php?cmd=ajax",
			    {
			    	subcmd: "table",
			        name: number,
			        type: type,
			        selected:selected,
			    },
			    function(data, status){
			      $( "#select_result"+number ).html(data);  
			    });
			}

			function append_columns(sel)
			{
				selected=$(sel).val();
				selectoption=sel.id;
				number=selectoption.replace(/\D/g,"");
				$.get("import.php?cmd=ajax",
			    {
			  		subcmd:"columns",
	  				postno:number,
			        selected:selected,
			    },
			    function(data, status){
			      $( "#column_details_div_"+number ).html(data);  
			    });
			}
			
			
			</script>
		<form action="" method="post">
		<table id="mytable"><tbody>
		<tr><td>Form Name</td><Td><input type="hidden" name="upload_id" value="'.$DATA['upload_id'].'"><input type="text" name="form_name" value="'.$form_name.'" placeholder="Form Name"></td><tr>
		<tr><td>Table Name</td><Td><input type="text" name="table_name" value="'.$table_name.'" placeholder="Form Name"></td><tr>
		<tr><td>List</td><td>Label</td><td>Variable</td><td>Mandatory</td><td>Validation</td><td>Type</td></tr>';
		$row=0;
		echo '<pre>';
		print_r($DATA);
		foreach($DATA['field_name'] as $key=>$field)
		{
			$field_var=clean(trim($field),true);
			$field_label=clean(trim($DATA['field_label'][$key]),false);
			$table_show=isset($DATA['table_show'][$key])?' checked="checked" ':'';
			$mandatory=isset($DATA['mandatory'][$key])?' checked="checked" ':'';
			$html.='<tr>
					<td> <input type="checkbox" name="table_show[]" value="1" '.$table_show.'> </td>
					<td> <input type="text" name="field_label[]" value="'.$field_label.'"> </td>
					<td> <input type="text" name="field_name[]" value="'.$field_var.'"> </td>
					

				    <td> <input type="checkbox" name="mandatory[]" value="1" '.$mandatory.'> </td>
				    <td> <select name="Validation[]">
							';
							$html.= get_validation_drop_down($DATA['Validation'][$key]);
							$html.='
				    	 </select>
				    </td>
					<td>
						<select name="type[]" id="type'.$row.'" onchange="select_changed(this)">
						';
						$html.= get_type_drop_down($DATA['type'][$key]);
						$html.='
						</select>
				    </td>';
				    $tablehtml='';
				    if(isset($DATA['table_name'.$key]))
				    {
				    	$tablehtml=get_table_details($key,$DATA['table_name'.$key]).'<div id="column_details_div_'.$key.'">';
				    	$tablehtml.=get_table_column_details($DATA['table_name'.$key],$key,$DATA['option_value'.$key],$DATA['option'.$key]);
				    	//$tablehtml.='</div>';
				    	
				    }

				    if(isset($DATA['radio_name'][$key]))
				    {
				    	$tablehtml.='<input type="text" name="radio_name['.$key.'][]" value="'.$DATA['radio_name'][$key][0].'">Pipe seperated';	
				    }
				    $html.='<td><div id="select_result'.$row.'" >'.$tablehtml.'</div></td>';
			    	$html.='</tr>';
			    	$row++;
		}
		$html.='<tbody></table><table>
		<tr><td> <button onclick="return add_column();">Add Column</button> </td></tr>
		<tr><td><input type="submit" name="submit_generate" value="Generate"></td></tr></table>
		<script>
		$( "tbody" ).sortable(); 
		function add_column()
			{
				$("#myTable > tbody:last-child").append(\'<tr><td> <input type="checkbox" name="table_show[]" value="1" checked="checked"> </td><td> <input type="text" name="field_label[]" value=""> </td><td> <input type="text" name="field_name[]" value=""> </td><td> <input type="checkbox" name="mandatory[]" value="1" > </td><td> <select name="Validation[]">'.get_validation_drop_down().'</select></td><td><select name="type[]" id="type" onchange="select_changed(this)">'.get_type_drop_down().'</select></td><td><div id="select_result" ></div></td>\');				return false;
			}
		</script> ';
		echo $html;
		exit;
}else if(isset($DATA['cmd']) and $DATA['cmd']=='columns_selected' )
{
	//$selected_columns=explode(",", $DATA['selected_columns']);
	$DATA['upload_id']=isset($DATA['upload_id'])?$DATA['upload_id']:0;
	if(is_array($DATA['selected_columns']))
		$selected_columns=$DATA['selected_columns'];
	else
		$selected_columns=json_decode($DATA['selected_columns']);
	$form_name=$DATA['form_name'];
	if($DATA['upload_id']==0)
	{
		$sql="insert into builder_uploads (form_name,columns,upload_time	) values ('".$DATA['form_name']."','".serialize($DATA)."', now())";
		$result=$mysqli->query($sql);
		$DATA['upload_id']=$mysqli->insert_id;
	}
	
	
	$table_name=clean($form_name,true);
	$html='
		<script src="../boot/js/jquery.min.js"></script>
		<script type="text/javascript">
		function select_changed(sel)
		{
			selected=sel.id;
			
			number=selected.replace(/\D/g,"");
			type=$(sel).val();
			$.get("import.php?cmd=ajax",
		    {
		    	subcmd: "table",
		        name: number,
		        type: type,
		        selected:selected,
		    },
		    function(data, status){
		      $( "#select_result"+number ).html(data);  
		    });
		}

		function append_columns(sel)
		{
			selected=$(sel).val();
			selectoption=sel.id;
			number=selectoption.replace(/\D/g,"");
			$.get("import.php?cmd=ajax",
		    {
		  		subcmd:"columns",
  				postno:number,
		        selected:selected,
		    },
		    function(data, status){
		      $( "#select_result"+number ).append(data);  
		    });
		}
		</script>
	<form action="" method="post">
	<table>
	<tr><td>Form Name</td><Td><input type="hidden" name="upload_id" value="'.$DATA['upload_id'].'"><input type="text" name="form_name" value="'.$form_name.'" placeholder="Form Name"></td><tr>
	<tr><td>Table Name</td><Td><input type="text" name="table_name" value="'.$table_name.'" placeholder="Form Name"></td><tr>
	<tr><td>List</td><td>Label</td><td>Variable</td><td>Mandatory</td><td>Validation</td><td>Type</td></tr>';
	$row=0;
	print_r($DATA);
	foreach($selected_columns as $key=>$field)
	{
		$field_var=clean(trim($field),true);
		$field_label=clean(trim($field),false);

		$mandatory=isset($DATA['mandatory'][$key])?' checked="checked" ':'';
		$html.='<tr>
				<td> <input type="checkbox" name="table_show[]" value="1" checked="checked"> </td>
				<td> <input type="text" name="field_label[]" value="'.$field_label.'"> </td>
				<td> <input type="text" name="field_name[]" value="'.$field_var.'"> </td>
			    <td> <input type="checkbox" name="mandatory[]" value="1" '.$mandatory.'> </td>
			    <td> <select name="Validation[]">
						';
						$html.= (get_validation_drop_down(0));
						$html.='
			    	 </select>
			    </td>
				<td>
					<select name="type[]" id="type'.$row.'" onchange="select_changed(this)">
					';
					$html.= get_type_drop_down(0);
					$html.='
					</select>
			    </td>';

		    	$html.='<td><div id="select_result'.$row.'" ></div></td>';
		    	$html.='</tr>';
		    	$row++;
	}
	$html.='<tr><td><input type="submit" name="submit_generate" value="Generate"></td></tr>';
	echo $html;
	exit;
}else if(isset($DATA['submit_csv']) and $DATA['submit_csv']=="Upload")
{
	if ( isset($_FILES["fileToUpload"])) 
	{
        if ($_FILES["fileToUpload"]["error"] > 0) 
        {
            echo "Return Code: " . $_FILES["fileToUpload"]["error"] . "<br />";
        }
        else 
        {
        	$uploaded_filename=clean($_FILES["fileToUpload"]["name"],false);
        	$filename="../upload/".time().'_'.$_FILES["fileToUpload"]["name"];
            move_uploaded_file($_FILES["fileToUpload"]["tmp_name"],  $filename);
            $mysqli->query('insert into builder_uploads (file_name) value ("'.$_FILES["fileToUpload"]["name"].'")');
            
			//echo print_excel_selectable_format($filename);

			include 'excel/PHPExcel/IOFactory.php';
			include 'excel/PHPExcel.php';
			//set_include_path(get_include_path() . PATH_SEPARATOR . '../../../excel/');
			//set_include_path("/Applications/XAMPP/xamppfiles/htdocs/pricol/excel/");
			$inputFileName = $filename;
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objReader->setLoadAllSheets();
			$objPHPExcel = $objReader->load($inputFileName);
			echo sheet_html();
			echo $html=read_sheet(0);
			echo sheet_postfix($uploaded_filename,$mysqli->insert_id);

        }
     } else 
     {
             echo "No file selected <br />";
     }
}elseif(isset($DATA['cmd']) and $DATA['cmd']=='simple')
{
	$html='<script src="../boot/js/jquery.min.js"></script>
			<form action="import.php" method="post">
				<input type="text" name="form_name" value="">
				<div id="addthis">
				<input type="text" name="selected_columns[]" id="test1"><input type="button" onclick="add_div();" value="+"><input type="button" onclick="return remove_div();" value="-">
				</div>
				<input type="submit" value="columns_selected" name="cmd">
			</form>
			<script type="text/javascript">
			var i=0;

			function add_div()
			{
				i++;
				$(\'#addthis\').append(\'<br><input type="text" name="selected_columns[]" id="text\'+i+\'"><input type="button" id="addbutton\'+i+\'" onclick="add_div();" value="+"><input type="button" id="removebutton\'+i+\'"  onclick="return remove_div(\'+i+\');" value="-">\');
			}
			function remove_div(id)
			{
				$("#text"+id).remove();
					$("#addbutton"+id).remove();
						$("#removebutton"+id).remove();

			}
			</script>';
			echo $html;exit;
}elseif(isset($DATA['cmd']) and $DATA['cmd']=='add_role')
{
	if(isset($DATA['save_role']) and strlen($DATA['role'])>0)
	{
		if(check_roles($DATA['role']))
		{
			 $sql="insert into builder_roles (role_name) values ('".$DATA['role']."')";
			 $mysqli->query($sql);
		}

	}
	$html='<form action="" method="post">Role<input type="text" name="role" value=""><input type="submit" name="save_role" value="save"></form>';
	echo $html;
}else
{
	$html='
	<link href="../boot/css/bootstrap.min.css" rel="stylesheet">
	<form action="" method="post" enctype="multipart/form-data">
    Select Csv to upload:
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="submit" value="Upload" name="submit_csv">
	</form>
	<a href="import.php?cmd=simple" class="btn btn-info" role="button">New module</a>
	<a href="import.php?cmd=add_role" class="btn btn-info" role="button">Add Role</a>
	';
	$result=$mysqli->query("select * from builder_uploads");
		$html.='<br><table>';
		
		
		while($line=$result->fetch_array(MYSQLI_ASSOC))
		{
			$html.='<tr><td><a href="import.php?cmd=module_selected&id='.$line['id'].'">'.$line['file_name'].$line['form_name'].'</a></td></tr>';
		}
		$html.='</table>';
	echo $html;
}


function clean($string,$isvar) 
{
	if($isvar)
	{
		$string=strtolower($string);
		$string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.
	}else{
		$string=ucwords($string);
	}
   return preg_replace('/[^A-Za-z0-9\_ ]/', '', $string); // Removes special chars.
}
function get_validation_drop_down($opt=0)
{

	$list = array('email','unique','mobile');
	$html='<option value="0">Select</option>';
	foreach($list as $item)
	{
		if(strcmp($opt,$item)==0)
			$html.='<option value="'.$item.'" selected="selected">'.ucwords($item).'</option>';
		else
			$html.='<option value="'.$item.'">'.ucwords($item).'</option>';
	}
	return  $html;
}
function get_type_drop_down($opt=0)
{
	$list=array('Text'=>'VARCHAR','Long Text'=>'TEXT','Number'=>'int','Date'=>'DATE','Datetime'=>'DATETIME','Radio'=>'radio','CheckBox'=>'check','Table'=>'table');
	$html='<option value="0">Select</option>';
	foreach($list as $key=>$item)
	{

		if(strcmp($opt,$item)==0)
			$html.='<option value="'.$item.'" selected="selected">'.ucwords($key).'</option>';
		else
			$html.='<option value="'.$item.'">'.ucwords($key).'</option>';
	}
	return  $html;
}

function check_roles($role)
{
	global $mysqli;
	$sql="select * from builder_roles where role_name='".$role."'";
	$result=$mysqli->query($sql);
	if($result->num_rows>0)
	{
		return false;
	}
	return true;
}
?>