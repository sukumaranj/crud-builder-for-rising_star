<?php
function get_column_details($type,$field_name,$lable,$key)
{
	global $DATA;
	$mandatory='';
	if(isset($DATA['mandatory'][$key]) and $DATA['mandatory'][$key]==1)
	{
		$mandatory=' required ';
	}
	$Validation_type='';
	if(isset($DATA['Validation'][$key]))
	{
		$Validation_type=$DATA['Validation'][$key];
	}
	$input_type_insert='';
	$input_type_edit='';

	switch ($Validation_type)
	{
		case "email":
			$input_type_edit=$input_type_insert=' type="email" ';
		break;
		case "number":
			$input_type_edit=$input_type_insert=' type="number" ';
		break;
		case "unique":
			global $php_filename;
			$input_type_insert=' type="text" data-remote="'.$php_filename.'?cmd=unique_check&table='.$DATA['table_name'].'&column='.$field_name.'" ';
			$input_type_edit=' type="text" data-remote="'.$php_filename.'?cmd=unique_check&table='.$DATA['table_name'].'&update=\'.$_GET["id"].\'&column='.$field_name.'"';
		break;
		default:
			$input_type_edit=$input_type_insert=' type="text" ';
		break;
	}

	switch ($type)
	{
		case "VARCHAR":
			$r['type']= $type.'(30)';
			$r['html']='<input tabindex="'.($key+1).'"  class="form-control" name="'.$field_name.'" id="'.$field_name.'" placeholder="Enter '.$lable.'" '.$mandatory.$input_type_insert.'>';
			$r['edit_html']='<input tabindex="'.($key+1).'"   class="form-control" name="'.$field_name.'" id="'.$field_name.'" placeholder="Enter '.$lable.'" value="\'.$row[\''.$field_name.'\'].\'"  '.$mandatory.$input_type_edit.'>';
			$r['search_html']='<input class="form-control" name="filter['.$field_name.']" id="'.$field_name.'">';
		break;
		case "int":
			$r['type']= 'int(30)';
			$r['html']='<input  tabindex="'.($key+1).'"  class="form-control" name="'.$field_name.'" id="'.$field_name.'" placeholder="Enter '.$lable.'" '.$mandatory.$input_type_insert.'>';
			$r['edit_html']='<input  tabindex="'.($key+1).'"  class="form-control" name="'.$field_name.'" id="'.$field_name.'" placeholder="Enter '.$lable.'" value="\'.$row[\''.$field_name.'\'].\'"  '.$mandatory.$input_type_edit.'>';
			$r['search_html']='<input class="form-control" name="filter['.$field_name.']" id="'.$field_name.'">';
		break;
		case "table":
			$table_name=$_POST['table_name'.$key];
			$id_column=$_POST['option_value'.$key];
			$value_column=$_POST['option'.$key];
			$r=get_option($key,$field_name,$table_name,$id_column,$value_column,$lable);
			$r['type']= 'int(30)';
		break;
		
		case "DATE":
		case "DATETIME":
			$r['type']= 'datetime';
			$r['html']='<div class="input-group date" id="datetimepicker'.$field_name.'"><input  tabindex="'.($key+1).'"  '.$mandatory.$input_type_insert.' class="form-control" name="'.$field_name.'" id="'.$field_name.'" ><span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span></div><script type="text/javascript">$(function () {$("#datetimepicker'.$field_name.'").datetimepicker({format: "YYYY-MM-DD hh:mm:00 a"});});</script>';
			$r['edit_html']='<div class="input-group date" id="datetimepicker'.$field_name.'"><input  tabindex="'.($key+1).'"  '.$mandatory.$input_type_edit.' class="form-control" name="'.$field_name.'" id="'.$field_name.'" ><span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span></div><script type="text/javascript">$(function () {$("#datetimepicker'.$field_name.'").datetimepicker({format: "YYYY-MM-DD hh:mm:00 a",defaultDate: "\'.$row[\''.$field_name.'\'].\'"});});</script>';
			$r['search_html']='<input class="form-control" name="filter['.$field_name.']" id="'.$field_name.'">';
			$r['search_javascript']=getrange_javascript($field_name);
			$r['search_javascript_call']='$("#'.$field_name.'").val(start.format("YYYY/M/D") + " - " + end.format("YYYY/M/D"));';

		break;
		case "radio":
			$r['type']= 'TEXT';
			$r['html']='<div class="radio">';
			$r['edit_html']='<div class="radio">';
			foreach(array_map('ucwords',(explode('|',$DATA['radio_name'][$key][0]))) as $value)
			{
				$r['html'].='<label class="radio-inline"><input  tabindex="'.($key+1).'"  type="radio" '.$mandatory.' id="'.$field_name.'"  name="'.$field_name.'" value="'.$value.'">'.$value.'</label>';
				$r['edit_html'].='<label class="radio-inline"><input  tabindex="'.($key+1).'"  type="radio" \'. (($row[\''.$field_name.'\']==\''.$value.'\')?\'checked="checked"\':\'\').\' '.$mandatory.' id="'.$field_name.'"  name="'.$field_name.'" value="'.$value.'">'.$value.'</label>';
			}
			$r['edit_html'].='</div>';
			$r['html'].='</div>';
			//'<textarea class="form-control"  '.$mandatory.$input_type_edit.' name="'.$field_name.'" id="'.$field_name.'" placeholder="Enter '.$lable.'" >\'.$row[\''.$field_name.'\'].\'</textarea>';
		break;
		case "TEXT":
			$r['type']= 'TEXT';
			$r['html']='<textarea  tabindex="'.($key+1).'" class="form-control"  '.$mandatory.$input_type_insert.' name="'.$field_name.'" id="'.$field_name.'" placeholder="Enter '.$lable.'"></textarea>';
			$r['edit_html']='<textarea  tabindex="'.($key+1).'" class="form-control"  '.$mandatory.$input_type_edit.' name="'.$field_name.'" id="'.$field_name.'" placeholder="Enter '.$lable.'" >\'.$row[\''.$field_name.'\'].\'</textarea>';
			$r['search_html']='<input class="form-control" name="filter['.$field_name.']" id="'.$field_name.'">';
		break;
		case "0":
			$r['type']= 'int(11)';
			$r['html']='<input class="form-control"  tabindex="'.($key+1).'"  '.$mandatory.$input_type_insert.' name="'.$field_name.'" id="'.$field_name.'" placeholder="Enter '.$lable.'">';
			$r['edit_html']='<input class="form-control"  tabindex="'.($key+1).'"  '.$mandatory.$input_type_edit.' name="'.$field_name.'" id="'.$field_name.'" placeholder="Enter '.$lable.'" value="\'.$row[\''.$field_name.'\'].\'">';
			$r['search_html']='<input class="form-control" name="['.$field_name.']" id="'.$field_name.'">';
		break;
		default:
			$r['type']= 'varchar(30)';
			$r['html']='<input class="form-control"  tabindex="'.($key+1).'"  '.$mandatory.$input_type_insert.' name="'.$field_name.'" id="'.$field_name.'" placeholder="Enter '.$lable.'">';
			$r['edit_html']='<input class="form-control"  tabindex="'.($key+1).'"  '.$mandatory.$input_type_edit.' name="'.$field_name.'" id="'.$field_name.'" placeholder="Enter '.$lable.'" value="\'.$row[\''.$field_name.'\'].\'">';
			$r['search_html']='<input class="form-control" name="filter['.$field_name.']" id="'.$field_name.'">';
		break;
	}
	return $r;
}
function get_option($key,$field_name,$table_name,$id_column,$value_column,$label)
{
	$temp['html']='<?php echo get_'.$field_name.'_drop_down("'.$id_column.'","'.$value_column.'",0) ?>';
	$temp['php']='function get_'.$field_name.'_drop_down($id_column,$value_column,$selected_value)
	{
		global $mysqli;
		$result=$mysqli->query("select * from '.$table_name.'");
		$html=\'<select class="form-control"  tabindex="'.($key+1).'"  name="'.$field_name.'" >
		<option value="0">Select one</option>\';
		while($line=$result->fetch_array(MYSQLI_ASSOC))
		{
			if($line[\''.$id_column.'\']==$selected_value)
				$html.=\'<option value="\'.$line["'.$id_column.'"].\'" selected="selected">\'.$line["'.$value_column.'"].\'</option>\';
			else
				$html.=\'<option value="\'.$line["'.$id_column.'"].\'">\'.$line["'.$value_column.'"].\'</option>\';
		}
		$html.=\'</select>\';
		return $html;
	}';
	$temp['edit_html']='\'.get_'.$field_name.'_drop_down("'.$id_column.'","'.$value_column.'",$row[\''.$field_name.'\']).\'';
	$temp['search_html']='\'.get_'.$field_name.'_drop_down("filter['.$id_column.']","'.$value_column.'",0).\'';
	return $temp;
	exit;
}
function getrange_javascript($fieldname)
{
	return '$("#'.$fieldname.'").daterangepicker({
						    	locale: {
								      format: "YYYY/MM/DD"
								},
						        startDate: start,
						        endDate: end,
						        ranges: {
						           "Last 7 Days": [moment().subtract(6, "days"), moment()],
						           "Last 30 Days": [moment().subtract(29, "days"), moment()],
						           "This Month": [moment().startOf("month"), moment().endOf("month")],
						           "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")],
						           "This Year": [moment().startOf("year"), moment().endOf("year")],
						           "Last Year": [moment().subtract(1, "year").startOf("year"), moment().subtract(1, "year").endOf("year")],
						           "Today": [moment(), moment()],
						           "Yesterday": [moment().subtract(1, "days"), moment().subtract(1, "days")]
						        }
						    }, cb);';
}

?>