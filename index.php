<?php
include('config.php');
if(isset($_SESSION['user_id']) and isset($_GET['logout']))
{
  unset($_SESSION['user_id']);
  session_destroy();
}
if(isset($_SESSION['user_id']) and $_SESSION['user_id']>0)
{
  header('Location:dashboard.php');
}
if(isset($_POST['submit']))
{
 $query="select * from builder_users where username='".$mysqli->escape_string($_POST['username'])."' and password='".$mysqli->escape_string($_POST['password'])."' and is_admin=1 limit 1";
  $result=$mysqli->query($query);
  if($result->num_rows==1)
  {
    while($line=$result->fetch_array(MYSQLI_ASSOC))
    { 
      $_SESSION['user_id']=$line['id'];
      $_SESSION['username']=$line['username'];
      header('Location:dashboard.php'); 
    }
    
  }else
  {
    
  }
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Login</title>
    <link href="boot/css/bootstrap.min.css" rel="stylesheet">
    <script src="boot/jquery.min.js"></script>
    <script src="boot/js/bootstrap.min.js"></script>
    <style>
body {
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #eee;
}

.form-signin {
  max-width: 330px;
  padding: 15px;
  margin: 0 auto;
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 10px;
}
.form-signin .checkbox {
  font-weight: normal;
}
.form-signin .form-control {
  position: relative;
  height: auto;
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="email"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
    </style>
  </head>

  <body>

    <div class="container">

      <form class="form-signin" action="" method="post">
        <center><h2 class="form-signin-heading">sign in</h2></center>
        <label for="inputEmail" class="sr-only">Username</label>
        <input type="text" name="username" id="inputusername" class="form-control" placeholder="Username" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
       <!-- <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div> -->
        <button class="btn btn-lg btn-primary btn-block" name="submit" type="submit">Sign in</button>
      </form>

    </div>
    
  </body>
</html>

