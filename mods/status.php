<?php 
	include_once("config.php");
	is_logged_In();
	
	function get_table_value($table,$column,$value,$return)
		{
			global $mysqli;
			$query="select ".$return." from ".$table." where ".$column."='".$value."' and is_deleted=0 limit 1";
			$temp=$mysqli->query($query)->fetch_array(MYSQLI_ASSOC);
			return $temp[$return];
		}
	$html="";
	
	if(isset($_GET['cmd']) and $_GET['cmd']=="unique_check")
	{
		$value=$_GET[$_GET['column']];
		$table=$_GET['table'] ;
		$column=$_GET['column'];
		$update=isset($_GET['update'])?" and id!='".$_GET['update']."'":'';
		$rsql="select * from $table  where $column='".$value."' ".$update." and is_deleted=0";
        $result=$mysqli->query($rsql);
        if($result->num_rows>0)
        	http_response_code(418);
    	else
    		http_response_code(200);
    	exit;
	}elseif(isset($_GET['cmd']) and $_GET['cmd']=="delete"){
		$rsql="update status set is_deleted=1 , deleted_on=now(), deleted_by=".$_SESSION['user_id']." where id='".$_GET['id']."'";
        $mysqli->query($rsql);
	}elseif(isset($_POST["insert_status"]))
	{
		$rsql="insert into status(status,details,type,created_on,created_by) values ('".$_POST['status']."','".$_POST['details']."','".$_POST['type']."',now(),".$_SESSION['user_id'].")";
        $mysqli->query($rsql);
	}elseif(isset($_POST["edit_status"]))
	{
		$record_id=$_POST['id'];
		$rsql="update status set 
		status= '".$_POST['status']."', details= '".$_POST['details']."', type= '".$_POST['type']."'
		,last_modified_on=now(),last_modified_by=".$_SESSION['user_id']."
		where id=".$record_id;
		
        $mysqli->query($rsql);
	}elseif(isset($_POST["import_status"]))
	{
		if (is_uploaded_file($_FILES["import_status"]["tmp_name"])) 
		{
			ini_set("auto_detect_line_endings", "1");
			$handle = fopen($_FILES["import_status"]["tmp_name"], "r");
			fgetcsv($handle, 10000, ",");
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				//$data['0']=get_table_value();
				$import_sql='INSERT into status(status,details,type) values(';
				array_walk($data,function(&$item){$item='"'.$item.'"';});
				$import_sql.=implode(',',$data).')';
				$mysqli->query($import_sql);
			}
			fclose($handle);
		}



	}elseif(isset($_GET['cmd']) and $_GET['cmd']=="export")
		{ 
			$header='"Status","Details","Type"';
			$sql="select status.status,status.details,status.type from status where status.is_deleted=0 ";
			$list_html="";
			if ($result=$mysqli->query($sql))
			  {
			  if($result->num_rows>0)
				  {
				  	$data="";
				    while($row=$result->fetch_array(MYSQLI_ASSOC))
				    {
				   	  $line = "";
					    foreach( $row as $value )
					    {                                            
					        if ( ( !isset( $value ) ) || ( $value == "" ) )
					        {
					            $value = ",";
					        }
					        else
					        {
					            $value = str_replace( '"' , '""' , $value );
					            $value = '"' . $value . '"' . ",";
					        }
					        $line .= $value;
					    }
					    $data .= trim( rtrim($line,',' ) ) . "\n";
					}
					$data = str_replace( "\r" , "" , $data );

					if ( $data == "" )
					{
					    $data = "\n(0) Records Found!\n";                        
					}

					header("Content-type: application/octet-stream");
					header("Content-Disposition: attachment; filename=status_".date("Y_m_d_H_i_s").".csv");
					header("Pragma: no-cache");
					header("Expires: 0");
					print "$header\n$data";
					exit;
				  }
			  }

		}elseif(isset($_GET['cmd']) and $_GET['cmd']=="jsondata")
		{
			$sql4="select * from reports where id=".$_GET['id'];
			if ($result4=$mysqli->query($sql4))
			  {
				  if($result4->num_rows>0)
				  {
				    while($row4=$result4->fetch_array(MYSQLI_ASSOC))
				    {
				    	$report_data=array();
					   	 $sql5s=explode(";",$row4['query']);
					   	 foreach($sql5s as $sql5)
					   	 {
					   	  if ($result5=$mysqli->query($sql5))
						  {
							  if(isset($result5->num_rows) and $result5->num_rows>0)
							  {
							    while($row5=$result5->fetch_array(MYSQLI_ASSOC))
							    {
							    	$report_data[]=$row5;
							    	$report_name=ucwords($row4['name']);
							    	$report_id=$row4['id'];
							    	
							    }
							    echo json_encode($report_data);exit;
							  }
						  }
						 }
				    }
				  }
			  }
		}
		elseif(isset($_GET['cmd']) and $_GET['cmd']=="view_report")
		{

			$sql4="select * from reports where id=".$_GET['id'];
			if ($result4=$mysqli->query($sql4))
			  {
				  if($result4->num_rows>0)
				  {
				    while($row4=$result4->fetch_array(MYSQLI_ASSOC))
				    {
				    	$report_data=array();
					   	 $sql5s=explode(";",$row4['query']);
					   	 foreach($sql5s as $sql5)
					   	 {
					   	 if ($result5=$mysqli->query($sql5))
						  {
							  if(isset($result5->num_rows) and $result5->num_rows>0)
							  {
							    while($row5=$result5->fetch_array(MYSQLI_ASSOC))
							    {
							    	$report_name=ucwords($row4['name']);
							    	$report_id=$row4['id'];
							    	$report_axis=$row4['script'];

							    }
							  }
						  }
						 }
				    }
				  }
			  }

		}elseif(isset($_GET['cmd']) and $_GET['cmd']=="new_report")
		{
			
		}elseif(isset($_GET['cmd']) and $_GET['cmd']=="edit_report")
		{
			
		}elseif(isset($_GET['cmd']) and $_GET['cmd']=="delete_report")
		{
			
		}
		
		$sql3="select * from reports where module_id='0'";
			$list_report_html='';
			if ($result3=$mysqli->query($sql3))
			  {
				  if($result3->num_rows>0)
				  {
				    while($row3=$result3->fetch_array(MYSQLI_ASSOC))
				    {
					   	 $list_report_html.="<tr>";
					     $list_report_html.= "<td>".$row3['name']."</td>";
					     $list_report_html.= "<td><a href=\"status.php?cmd=view_report&id=".$row3["id"]."\"><span aria-hidden=\"true\" class=\"glyphicon glyphicon-eye-open\"></span></a></td>";
					     $list_report_html.= "<td><a href=\"status.php?cmd=edit_report&id=".$row3["id"]."\"><span aria-hidden=\"true\" class=\"glyphicon glyphicon-edit\"></span></a></td>";
					     $list_report_html.= "<td><a href=\"status.php?cmd=delete_report&id=".$row3["id"]."\"><span aria-hidden=\"true\" class=\"glyphicon glyphicon-trash\"></span></a></td>";
					     $list_report_html.="</tr>";
				    }
				  }
			  }
	?>
	<!DOCTYPE html>
		<html lang="en">
		<head>
		  <title>status</title>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		 <link href="../boot/css/bootstrap.min.css" rel="stylesheet">
		 <link rel="stylesheet" type="text/css" href="../boot/css/c3.css">
		<script src="../boot/js/jquery.min.js"></script>
		<script src="../boot/js/moment.js"></script>
		<script src="../boot/js/bootstrap.min.js"></script>
		<script src="../boot/js/bootstrap-datetimepicker.js"></script>
		<script src="../boot/js/d3.js" charset="utf-8"></script>
    	<script src="../boot/js/c3.js"></script>
    	<script src="../boot/js/validator.js"></script>
    	
		</head>
		<body>

		<?php
		include_once("nav.php");

			if(isset($_GET['cmd']) and $_GET['cmd']=="view_report")
			{
				?>
			<div class="container">
			  <h2>Status</h2>
			  <p><?php echo $report_name; ?><a href="status.php" class="btn btn-info" role="button">List</a>
			   <a href="status.php?cmd=report" class="btn btn-info" role="button">Reports</a>
			  </p>            
			  <div id="chart"></div>
			  <script>
				var chart = c3.generate({
				          data: {
				            url: "status.php?cmd=jsondata&id=<?php echo $report_id; ?>",
				            mimeType: "json", 
				            <?php echo $report_axis;?>
				          
				        });
				</script>
			</div>
			<?php 
				
			}elseif(isset($_GET['cmd']) and $_GET['cmd']=="insert"){
			$id=0;
			?>
			<div class="container">
			  <h2>Status</h2>
			  <p>Add New Status <a href="status.php" class="btn btn-info" role="button">List</a></p>            
			  <form class="form-horizontal" method="post"  data-toggle="validator">
			  <div class="form-group">
						      <label class="control-label col-sm-2" for="status">Status:</label>
						      <div class="col-sm-4">
						        <input  class="form-control" name="status" id="status" placeholder="Enter Status"  required  type="text" data-remote="status.php?cmd=unique_check&table=status&column=status" >
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="details">Details:</label>
						      <div class="col-sm-4">
						        <textarea class="form-control"   type="text"  name="details" id="details" placeholder="Enter Details"></textarea>
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="type">Type:</label>
						      <div class="col-sm-4">
						        <div class="radio"><label class="radio-inline"><input type="radio" '.(($row['type']=='Yes')?'checked="checked"':'').'   id="type"  name="type" value="Yes">Yes</label><label class="radio-inline"><input type="radio" '.(($row['type']=='No')?'checked="checked"':'').'   id="type"  name="type" value="No">No</label><label class="radio-inline"><input type="radio" '.(($row['type']=='DOnt')?'checked="checked"':'').'   id="type"  name="type" value="DOnt">DOnt</label></div>
						      </div>
						    </div>
			<div class="form-group">        
			      <div class="col-sm-offset-2 col-sm-10">
			        <button type="submit" name="insert_status" class="btn btn-default">Submit</button>
			      </div>
			    </div>
			  </form>
			</div>
			<?php 
		}elseif(isset($_GET['cmd']) and $_GET['cmd']=="edit")
		{
			$id=$_GET["id"];
			$sql="select * from status where id=$id";
			$form_html="";
			if ($result=$mysqli->query($sql))
			  {
			  if($result->num_rows>0)
			  {
			    while($row=$result->fetch_array(MYSQLI_ASSOC))
			    {
			    	$html='<input type="hidden" name="id" value="'.$id.'"><div class="form-group">
						      <label class="control-label col-sm-2" for="status">Status:</label>
						      <div class="col-sm-4">
						        <input  class="form-control" name="status" id="status" placeholder="Enter Status" value="'.$row['status'].'"   required  type="text" data-remote="status.php?cmd=unique_check&table=status&update='.$_GET["id"].'&column=status">
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="details">Details:</label>
						      <div class="col-sm-4">
						        <textarea class="form-control"   type="text"  name="details" id="details" placeholder="Enter Details" >'.$row['details'].'</textarea>
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="type">Type:</label>
						      <div class="col-sm-4">
						        <div class="radio"><label class="radio-inline"><input type="radio"  id="type"  name="type" value="Yes">Yes</label><label class="radio-inline"><input type="radio"  id="type"  name="type" value="No">No</label><label class="radio-inline"><input type="radio"  id="type"  name="type" value="DOnt">DOnt</label></div>
						      </div>
						    </div>';
			    }
			  }
			  }
			?>
			<div class="container">
			  <h2>Status</h2>
			  <p>Edit Status <a href="status.php" class="btn btn-info" role="button">List</a></p>            
			  <form class="form-horizontal" method="post"  data-toggle="validator">
			  <?php echo $html;?>
			<div class="form-group">        
			      <div class="col-sm-offset-2 col-sm-10">
			        <button type="submit" name="edit_status" class="btn btn-default">Submit</button>
			      </div>
			    </div>
			  </form>
			</div>
			<?php 

		}elseif(isset($_GET['cmd']) and $_GET['cmd']=="import")
		{
			?>
			<div class="container">
			  <h2>Status</h2>
			  <p>Import to Status <a href="status.php" class="btn btn-info" role="button">List</a>
				<a href="status.php?cmd=import" class="btn btn-info" role="button">Import</a>
			  </p>            
			  <form  enctype="multipart/form-data"  class="form-horizontal" method="post">
			  
			  <div class="form-group">        
			      <div class="col-sm-offset-2 col-sm-10">
			        <input type="file" name="import_status" class="btn btn-default">
			      </div>
			    </div>
			<div class="form-group">        
			      <div class="col-sm-offset-2 col-sm-10">
			        <button type="submit" name="import_status" class="btn btn-default">Submit</button>
			      </div>
			    </div>
			  </form>
			</div>
			<?php 

		}elseif(isset($_GET['cmd']) and $_GET['cmd']=="report")
		{ ?>
			<div class="container">
			  <h2>Status</h2>
			  <p>Reports Status 
			  <a href="status.php" class="btn btn-info" role="button">List</a>
			  <a href="status.php?cmd=new_report" class="btn btn-info" role="button">New</a>
			  </p>            
			  <table class="table">
			    <thead>
			      <tr>
			        <th>List</th><th>View</th><th>Edit</th><th>Delete</th>
			       </tr>
			    </thead>
			    <tbody>
			     <?php echo $list_report_html;?>
			    </tbody>
			  </table>
			</div><?php
		}
		else{
			$sql_count="select *  from status where status.is_deleted=0  ";
			$limit=10;
			$page=isset($_GET['page'])?$_GET['page']:1;
			$pagination_html="";
			$limit_sql="";
			if ($result_count=$mysqli->query($sql_count))
			  {
				  if($result_count->num_rows>$limit )
				  {
					  	$limit_sql=" limit ".($page-1)*$limit.",".$limit;
					  	$total_pages = ceil($result_count->num_rows / $limit);
					  	$pagination_html='<div class="container"  style="float:right;width:auto;"><ul class="pagination">';
					  	for($i=1;$i<=$total_pages;$i++)
					  		$pagination_html.='<li><a href="status.php?page='.$i.'">'.$i.'</a></li>';
					  	$pagination_html.='</ul></div>';
				  }
			  }

			$sql="select status.* from status where status.is_deleted=0  order by id desc".$limit_sql;
			$list_html="";
			if ($result=$mysqli->query($sql))
			  {
			  if($result->num_rows>0)
			  {
			    while($row=$result->fetch_array(MYSQLI_ASSOC))
			    {
				   	 $list_html.="<tr>";
				     $list_html.= "<td>".$row["status"]."</td><td>".$row["details"]."</td><td>".$row["type"]."</td>";
				     $list_html.= "<td><a href=\"status.php?cmd=edit&id=".$row["id"]."\"><span aria-hidden=\"true\" class=\"glyphicon glyphicon-edit\"></span></a></td>";
				     $list_html.= "<td><a href=\"status.php?cmd=delete&id=".$row["id"]."\"><span aria-hidden=\"true\" class=\"glyphicon glyphicon-trash\"></span></a></td>";
				     $list_html.="</tr>";
			    }
			  }
			  }

			
			?>
			<div class="container">
			  <h2>Status</h2>
			  <p>A List of Status <a href="status.php?cmd=insert" class="btn btn-info" role="button">Add</a>
				<a href="status.php?cmd=import" class="btn btn-info" role="button">Import</a>
				<a href="status.php?cmd=export" class="btn btn-info" role="button">Export</a>
				<a href="status.php?cmd=report" class="btn btn-info" role="button">Report</a>
			  </p>            
			  <table class="table">
			    <thead>
			      <tr>
			        <th>Status</th><th>Details</th><th>Type</th>
			       </tr>
			    </thead>
			    <tbody>
			     <?php echo $list_html;?>
			    </tbody>
			  </table>
			  <?php echo $pagination_html;?>
			</div>
			<?php
		}
		?>
		<br>
		
		</div>
		</body>
		</html> 