<?php 
	include_once("config.php");
	is_logged_In();
	
	function get_table_value($table,$column,$value,$return)
		{
			global $mysqli;
			$query="select ".$return." from ".$table." where ".$column."='".$value."' and is_deleted=0 limit 1";
			$temp=$mysqli->query($query)->fetch_array(MYSQLI_ASSOC);
			return $temp[$return];
		}
	$html="";
	
	if(isset($_GET['cmd']) and $_GET['cmd']=="unique_check")
	{
		$value=$_GET[$_GET['column']];
		$table=$_GET['table'] ;
		$column=$_GET['column'];
		$update=isset($_GET['update'])?" and id!='".$_GET['update']."'":'';
		$rsql="select * from $table  where $column='".$value."' ".$update." and is_deleted=0";
        $result=$mysqli->query($rsql);
        if($result->num_rows>0)
        	http_response_code(418);
    	else
    		http_response_code(200);
    	exit;
	}elseif(isset($_GET['cmd']) and $_GET['cmd']=="delete"){
		$rsql="update monthly_sum set is_deleted=1 , deleted_on=now(), deleted_by=".$_SESSION['user_id']." where id='".$_GET['id']."'";
        $mysqli->query($rsql);
	}elseif(isset($_POST["insert_monthly_sum"]))
	{
		$rsql="insert into monthly_sum(month,project,model,mtarget,ctarget,ca,percentage,delta,created_on,created_by) values ('".$_POST['month']."','".$_POST['project']."','".$_POST['model']."','".$_POST['mtarget']."','".$_POST['ctarget']."','".$_POST['ca']."','".$_POST['percentage']."','".$_POST['delta']."',now(),".$_SESSION['user_id'].")";
        $mysqli->query($rsql);
	}elseif(isset($_POST["edit_monthly_sum"]))
	{
		$record_id=$_POST['id'];
		$rsql="update monthly_sum set 
		month= '".$_POST['month']."', project= '".$_POST['project']."', model= '".$_POST['model']."', mtarget= '".$_POST['mtarget']."', ctarget= '".$_POST['ctarget']."', ca= '".$_POST['ca']."', percentage= '".$_POST['percentage']."', delta= '".$_POST['delta']."'
		,last_modified_on=now(),last_modified_by=".$_SESSION['user_id']."
		where id=".$record_id;
		
        $mysqli->query($rsql);
	}elseif(isset($_POST["import_monthly_sum"]))
	{
		if (is_uploaded_file($_FILES["import_monthly_sum"]["tmp_name"])) 
		{
			ini_set("auto_detect_line_endings", "1");
			$handle = fopen($_FILES["import_monthly_sum"]["tmp_name"], "r");
			{
			fgetcsv($handle, 10000, ",");
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				//$data['0']=get_table_value();
				$import_sql='INSERT into monthly_sum(month,project,model,mtarget,ctarget,ca,percentage,delta) values(';
				array_walk($data,function(&$item){$item='"'.$item.'"';});
				$import_sql.=implode(',',$data).')';
				$mysqli->query($import_sql);
			}
			$import_message='<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert">&times;</a><strong>Success!</strong> Import completed successfully.</div>';
			fclose($handle);
			}
		}



	}elseif(isset($_GET['cmd']) and $_GET['cmd']=="export")
		{ 
			$header='"Month","Project","Model","Mtarget","Ctarget","Ca","Percentage","Delta"';
			$sql="select monthly_sum.month,monthly_sum.project,monthly_sum.model,monthly_sum.mtarget,monthly_sum.ctarget,monthly_sum.ca,monthly_sum.percentage,monthly_sum.delta from monthly_sum where monthly_sum.is_deleted=0 ";
			$list_html="";
			if ($result=$mysqli->query($sql))
			  {
			  if($result->num_rows>0)
				  {
				  	$data="";
				    while($row=$result->fetch_array(MYSQLI_ASSOC))
				    {
				   	  $line = "";
					    foreach( $row as $value )
					    {                                            
					        if ( ( !isset( $value ) ) || ( $value == "" ) )
					        {
					            $value = ",";
					        }
					        else
					        {
					            $value = str_replace( '"' , '""' , $value );
					            $value = '"' . $value . '"' . ",";
					        }
					        $line .= $value;
					    }
					    $data .= trim( rtrim($line,',' ) ) . "\n";
					}
					$data = str_replace( "\r" , "" , $data );

					if ( $data == "" )
					{
					    $data = "\n(0) Records Found!\n";                        
					}

					header("Content-type: application/octet-stream");
					header("Content-Disposition: attachment; filename=monthly_sum_".date("Y_m_d_H_i_s").".csv");
					header("Pragma: no-cache");
					header("Expires: 0");
					print "$header\n$data";
					exit;
				  }
			  }

		}elseif(isset($_GET['cmd']) and $_GET['cmd']=="jsondata")
		{
			$sql4="select * from builder_reports where id=".$_GET['id'];
			if ($result4=$mysqli->query($sql4))
			  {
				  if($result4->num_rows>0)
				  {
				    while($row4=$result4->fetch_array(MYSQLI_ASSOC))
				    {
				    	$report_data=array();
					   	 $sql5s=explode(";",$row4['query']);
					   	 foreach($sql5s as $sql5)
					   	 {

					   	  if ($result5=$mysqli->query($sql5))
						  {
							  if(isset($result5->num_rows) and $result5->num_rows>0)
							  {
							    while($row5=$result5->fetch_array(MYSQLI_ASSOC))
							    {
							    	$report_data[]=$row5;
							    	$report_name=ucwords($row4['name']);
							    	$report_id=$row4['id'];
							    	
							    }
							    echo json_encode($report_data);exit;
							  }
						  }
						 }
				    }
				  }
			  }
		}
		elseif(isset($_GET['cmd']) and $_GET['cmd']=="view_report")
		{

			$sql4="select * from builder_reports where id=".$_GET['id'];
			if ($result4=$mysqli->query($sql4))
			  {
				  if($result4->num_rows>0)
				  {
				    while($row4=$result4->fetch_array(MYSQLI_ASSOC))
				    {
				    	$report_data=array();
					   	 $sql5s=explode(";",$row4['query']);
					   	 //echo $sql5s[0];
					   	 foreach($sql5s as $sql5)
					   	 {
					   	 	//echo $sql5;
					   	 if ($result5=$mysqli->query($sql5))
						  {

							  if(isset($result5->num_rows) and $result5->num_rows>0)
							  {
							    while($row5=$result5->fetch_array(MYSQLI_ASSOC))
							    {
							    	$report_name=ucwords($row4['name']);
							    	$report_id=$row4['id'];
							    	$report_axis=$row4['script'];
							    	//$report_axis='';

							    }
							  }
						  }
						 }
				    }
				  }
			  }

		}elseif(isset($_GET['cmd']) and $_GET['cmd']=="new_report")
		{
			
		}elseif(isset($_GET['cmd']) and $_GET['cmd']=="edit_report")
		{
			
		}elseif(isset($_GET['cmd']) and $_GET['cmd']=="delete_report")
		{
			
		}
		
		$sql3="select * from builder_reports where module_id='1'";
			$list_report_html='';
			if ($result3=$mysqli->query($sql3))
			  {
				  if($result3->num_rows>0)
				  {
				    while($row3=$result3->fetch_array(MYSQLI_ASSOC))
				    {
					   	 $list_report_html.="<tr>";
					     $list_report_html.= "<td>".$row3['name']."</td>";
					     $list_report_html.= "<td><a href=\"monthly_sum.php?cmd=view_report&id=".$row3["id"]."\"><span aria-hidden=\"true\" class=\"glyphicon glyphicon-eye-open\"></span></a></td>";
					     $list_report_html.= "<td><a href=\"monthly_sum.php?cmd=edit_report&id=".$row3["id"]."\"><span aria-hidden=\"true\" class=\"glyphicon glyphicon-edit\"></span></a></td>";
					     $list_report_html.= "<td><a href=\"monthly_sum.php?cmd=delete_report&id=".$row3["id"]."\"><span aria-hidden=\"true\" class=\"glyphicon glyphicon-trash\"></span></a></td>";
					     $list_report_html.="</tr>";
				    }
				  }
			  }
	?>
	<!DOCTYPE html>
		<html lang="en">
		<head>
		  <title>monthly_sum</title>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		 <link href="../boot/css/bootstrap.min.css" rel="stylesheet">
		 <link rel="stylesheet" type="text/css" href="../boot/css/c3.css">
		 <link rel="stylesheet" type="text/css" href="../boot/css/daterangepicker.css" />
		<script src="../boot/js/jquery.min.js"></script>
		<script src="../boot/js/moment.js"></script>
		<script src="../boot/js/daterangepicker.js"></script>
		<script src="../boot/js/bootstrap.min.js"></script>
		<script src="../boot/js/bootstrap-datetimepicker.js"></script>
		<script src="../boot/js/d3.js" charset="utf-8"></script>
    	<script src="../boot/js/c3.js"></script>
    	<script src="../boot/js/validator.js"></script>
    	
		</head>
		<body>

		<?php
		include_once("nav.php");

			if(isset($_GET['cmd']) and $_GET['cmd']=="view_report")
			{
				?>
			<div class="container">
			  <h2>Monthly Sum</h2>
			  <p><?php echo $report_name; ?><a href="monthly_sum.php" class="btn btn-info" role="button"   accesskey="l">List</a>
			   <a href="monthly_sum.php?cmd=report" class="btn btn-info" role="button">Reports</a>
			  </p>            
			  <div id="chart"></div>
			  <script>
				var chart = c3.generate({
				          data: {
				            url: "monthly_sum.php?cmd=jsondata&id=<?php echo $report_id; ?>",
				            mimeType: "json", 
				            
				            <?php echo $report_axis;?>
				          
				        });
				</script>
			</div>
			<?php 
				
			}elseif(isset($_GET['cmd']) and $_GET['cmd']=="insert"){
			$id=0;
			?>
			<div class="container">
			  <h2>Monthly Sum</h2>
			  <p>Add New Monthly Sum <a href="monthly_sum.php" class="btn btn-info" role="button"   accesskey="l">List</a></p>            
			  <form class="form-horizontal" method="post"  data-toggle="validator">
			  <div class="form-group">
						      <label class="control-label col-sm-2" for="month">Month:</label>
						      <div class="col-sm-4">
						        <input tabindex="1"  class="form-control" name="month" id="month" placeholder="Enter Month"  type="text" >
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="project">Project:</label>
						      <div class="col-sm-4">
						        <input tabindex="2"  class="form-control" name="project" id="project" placeholder="Enter Project"  type="text" >
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="model">Model:</label>
						      <div class="col-sm-4">
						        <input tabindex="3"  class="form-control" name="model" id="model" placeholder="Enter Model"  type="text" >
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="mtarget">Mtarget:</label>
						      <div class="col-sm-4">
						        <input class="form-control"  tabindex="4"   type="text"  name="mtarget" id="mtarget" placeholder="Enter Mtarget">
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="ctarget">Ctarget:</label>
						      <div class="col-sm-4">
						        <input class="form-control"  tabindex="5"   type="text"  name="ctarget" id="ctarget" placeholder="Enter Ctarget">
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="ca">Ca:</label>
						      <div class="col-sm-4">
						        <input class="form-control"  tabindex="6"   type="text"  name="ca" id="ca" placeholder="Enter Ca">
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="percentage">Percentage:</label>
						      <div class="col-sm-4">
						        <input class="form-control"  tabindex="7"   type="text"  name="percentage" id="percentage" placeholder="Enter Percentage">
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="delta">Delta:</label>
						      <div class="col-sm-4">
						        <input class="form-control"  tabindex="8"   type="text"  name="delta" id="delta" placeholder="Enter Delta">
						      </div>
						    </div>
			<div class="form-group">        
			      <div class="col-sm-offset-2 col-sm-10">
			        <button type="submit" name="insert_monthly_sum" class="btn btn-default">Submit</button>
			      </div>
			    </div>
			  </form>
			</div>
			<?php 
		}elseif(isset($_GET['cmd']) and $_GET['cmd']=="view")
		{
			$id=$_GET["id"];
			$sql="select * from monthly_sum where id=$id";
			$form_html="";
			if ($result=$mysqli->query($sql))
			  {
			  if($result->num_rows>0)
			  {
			    while($row=$result->fetch_array(MYSQLI_ASSOC))
			    {
			    	$html='<div class="form-group">
						      <label class="control-label col-sm-2" for="month">Month:</label>
						      <div class="col-sm-4">
						        '.$row["month"].'
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="project">Project:</label>
						      <div class="col-sm-4">
						        '.$row["project"].'
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="model">Model:</label>
						      <div class="col-sm-4">
						        '.$row["model"].'
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="mtarget">Mtarget:</label>
						      <div class="col-sm-4">
						        '.$row["mtarget"].'
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="ctarget">Ctarget:</label>
						      <div class="col-sm-4">
						        '.$row["ctarget"].'
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="ca">Ca:</label>
						      <div class="col-sm-4">
						        '.$row["ca"].'
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="percentage">Percentage:</label>
						      <div class="col-sm-4">
						        '.$row["percentage"].'
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="delta">Delta:</label>
						      <div class="col-sm-4">
						        '.$row["delta"].'
						      </div>
						    </div>';
			    }
			  }
			  }
			?>
			<div class="container">
			  <h2>Monthly Sum</h2>
			  <p>View Monthly Sum <a href="monthly_sum.php" class="btn btn-info" role="button"   accesskey="l">List</a></p>            
			  
			  <?php echo $html;?>
			<div class="form-group">        
			      <div class="col-sm-offset-2 col-sm-10">
				      <a href="monthly_sum.php?cmd=edit&id=<?php echo $id;?>" class="btn btn-warning" role="button">Edit</a>
				      <a href="monthly_sum.php?cmd=delete&id=<?php echo $id;?>" class="btn btn-danger" role="button">Delete</a> 
			      </div>
			    </div>
			  </form>
			</div>
			<?php 

		}
		elseif(isset($_GET['cmd']) and $_GET['cmd']=="edit")
		{
			$id=$_GET["id"];
			$sql="select * from monthly_sum where id=$id";
			$form_html="";
			if ($result=$mysqli->query($sql))
			  {
			  if($result->num_rows>0)
			  {
			    while($row=$result->fetch_array(MYSQLI_ASSOC))
			    {
			    	$html='<input type="hidden" name="id" value="'.$id.'"><div class="form-group">
						      <label class="control-label col-sm-2" for="month">Month:</label>
						      <div class="col-sm-4">
						        <input tabindex="1"   class="form-control" name="month" id="month" placeholder="Enter Month" value="'.$row['month'].'"   type="text" >
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="project">Project:</label>
						      <div class="col-sm-4">
						        <input tabindex="2"   class="form-control" name="project" id="project" placeholder="Enter Project" value="'.$row['project'].'"   type="text" >
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="model">Model:</label>
						      <div class="col-sm-4">
						        <input tabindex="3"   class="form-control" name="model" id="model" placeholder="Enter Model" value="'.$row['model'].'"   type="text" >
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="mtarget">Mtarget:</label>
						      <div class="col-sm-4">
						        <input class="form-control"  tabindex="4"   type="text"  name="mtarget" id="mtarget" placeholder="Enter Mtarget" value="'.$row['mtarget'].'">
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="ctarget">Ctarget:</label>
						      <div class="col-sm-4">
						        <input class="form-control"  tabindex="5"   type="text"  name="ctarget" id="ctarget" placeholder="Enter Ctarget" value="'.$row['ctarget'].'">
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="ca">Ca:</label>
						      <div class="col-sm-4">
						        <input class="form-control"  tabindex="6"   type="text"  name="ca" id="ca" placeholder="Enter Ca" value="'.$row['ca'].'">
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="percentage">Percentage:</label>
						      <div class="col-sm-4">
						        <input class="form-control"  tabindex="7"   type="text"  name="percentage" id="percentage" placeholder="Enter Percentage" value="'.$row['percentage'].'">
						      </div>
						    </div><div class="form-group">
						      <label class="control-label col-sm-2" for="delta">Delta:</label>
						      <div class="col-sm-4">
						        <input class="form-control"  tabindex="8"   type="text"  name="delta" id="delta" placeholder="Enter Delta" value="'.$row['delta'].'">
						      </div>
						    </div>';
			    }
			  }
			  }
			?>
			<div class="container">
			  <h2>Monthly Sum</h2>
			  <p>Edit Monthly Sum <a href="monthly_sum.php" class="btn btn-info" role="button"  accesskey="l">List</a></p>            
			  <form class="form-horizontal" method="post"  data-toggle="validator">
			  <?php echo $html;?>
			<div class="form-group">        
			      <div class="col-sm-offset-2 col-sm-10">
			        <button type="submit" name="edit_monthly_sum" class="btn btn-default">Submit</button>
			      </div>
			    </div>
			  </form>
			</div>
			<?php 

		}elseif(isset($_GET['cmd']) and $_GET['cmd']=="import")
		{
			?>
			<div class="container">
			  <h2>Monthly Sum</h2>
			  <p>Import to Monthly Sum <a href="monthly_sum.php" class="btn btn-info" role="button"  accesskey="l">List</a>
				<a href="monthly_sum.php?cmd=import" class="btn btn-info" role="button">Import</a>
			  </p>            
			  <form  enctype="multipart/form-data"  class="form-horizontal" method="post">
			  <?php echo isset($import_message)?$import_message:'';?>
			  <div class="form-group">        
			      <div class="col-sm-offset-2 col-sm-10">
			        <input type="file" name="import_monthly_sum" class="btn btn-default">
			      </div>
			    </div>
			<div class="form-group">        
			      <div class="col-sm-offset-2 col-sm-10">
			        <button type="submit" name="import_monthly_sum" class="btn btn-default">Submit</button>
			      </div>
			    </div>
			  </form>
			</div>
			<?php 

		}elseif(isset($_GET['cmd']) and $_GET['cmd']=="report")
		{ ?>
			<div class="container">
			  <h2>Monthly Sum</h2>
			  <p>Reports Monthly Sum 
			  <a href="monthly_sum.php" class="btn btn-info" role="button">List</a>
			  <a href="monthly_sum.php?cmd=new_report" class="btn btn-info" role="button">New</a>
			  </p>            
			  <table class="table">
			    <thead>
			      <tr>
			        <th>List</th><th>View</th><th>Edit</th><th>Delete</th>
			       </tr>
			    </thead>
			    <tbody>
			     <?php echo $list_report_html;?>
			    </tbody>
			  </table>
			</div><?php
		}elseif(isset($_GET['cmd']) and $_GET['cmd']=="search")
		{
			$filter=$_GET["filter"];
			$sql_where_bit="";
			if(isset($filter["mode"]) and strlen($filter["mode"]))
			{
				$sql_where_bit.=' and complaint_receipt_mode.mode="'.$filter["mode"].'"';
			}
			if(isset($filter["created_date"]) and strlen($filter["created_date"])>0)
			{
				$filter["created_date"]=str_replace("/","-",explode("-",$filter["created_date"]));
				$filter["created_date"]=array_map("trim", $filter["created_date"]);
			}
				$sql_where_bit.=' and complaint_receipt_mode.created_date between "'.$filter["created_date"][0].'" and "'.$filter["created_date"][1].'"';

			echo $sql_count="select *  from monthly_sum where monthly_sum.is_deleted=0  ".$sql_where_bit;
			
			
			$limit=10;
			$page=isset($_GET['page'])?$_GET['page']:1;
			$pagination_html="";
			$limit_sql="";
			if ($result_count=$mysqli->query($sql_count))
			  {
				  if($result_count->num_rows>$limit )
				  {
					  	$limit_sql=" limit ".($page-1)*$limit.",".$limit;
					  	$total_pages = ceil($result_count->num_rows / $limit);
					  	$pagination_html='<div class="container"  style="float:right;width:auto;"><ul class="pagination">';
					  	if($page>1)
					  	{
					  		$pagination_html.='<li class="page-item"><a class="page-link" href="monthly_sum.php?page=1" aria-label="Previous"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>';	
					  	}
					  	
					  	for($i=(($page-1)>0?($page-1):1);$i<=(($page+1)<$total_pages?$page+1:$total_pages);$i++)
					  	{
					  		if($page==$i)
					  			$pagination_html.='<li class="page-item active"><span class="page-link">'.$page.' <span class="sr-only">(current)</span></span></li>';
					  		else
					  			$pagination_html.='<li><a href="monthly_sum.php?page='.$i.'">'.$i.'</a></li>';
					  	}
					  		

					  	if($page<$total_pages)
					  	{
					  		$pagination_html.='<li class="page-item"><a class="page-link" href="monthly_sum.php?page='.($total_pages).'" aria-label="Next"><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>';	
					  	}
					  	
					  	
					  	$pagination_html.='</ul></div>';
				  }
			  }

		
			$sort_by=isset($_GET['sort'])?$_GET['sort']:"id";
			$order=isset($_GET['order'])?$_GET['order']:"asc";
			$sql="select monthly_sum.* from monthly_sum where monthly_sum.is_deleted=0  ".$sql_where_bit." order by monthly_sum.".$sort_by." ".$order.$limit_sql;
			$list_html="";
			if ($result=$mysqli->query($sql))
			  {
			  if($result->num_rows>0)
			  {
			    while($row=$result->fetch_array(MYSQLI_ASSOC))
			    {
				   	 $list_html.="<tr>";
				     $list_html.= "<td>".$row["month"]."</td><td>".$row["project"]."</td><td>".$row["model"]."</td><td>".$row["mtarget"]."</td><td>".$row["ctarget"]."</td><td>".$row["ca"]."</td><td>".$row["percentage"]."</td><td>".$row["delta"]."</td>";
				     $list_html.= "<td><a href=\"monthly_sum.php?cmd=view&id=".$row["id"]."\"><span aria-hidden=\"true\" class=\"glyphicon glyphicon-eye-open\"></span></a></td>";
				     $list_html.= "<td><a href=\"monthly_sum.php?cmd=edit&id=".$row["id"]."\"><span aria-hidden=\"true\" class=\"glyphicon glyphicon-edit\"></span></a></td>";
				     $list_html.= "<td><a href=\"monthly_sum.php?cmd=delete&id=".$row["id"]."\"><span aria-hidden=\"true\" class=\"glyphicon glyphicon-trash\"></span></a></td>";
				     $list_html.="</tr>";
			    }
			  }
			  }
			?>
			<div class="container">
			  <h2 data-toggle="collapse" data-target="#monthly_sum_main_collapse">Monthly Sum</h2>
			  <div id="monthly_sum_main_collapse" class="collapse">
			  	<button class="btn btn-info" role="button"  tabindex="-1"  data-toggle="collapse" data-target="#monthly_sum_search_collapse" ><span class="glyphicon glyphicon-search"></span> Search</button>
			  	<a class="btn btn-info" role="button"  tabindex="-1" href="monthly_sum.php?cmd=insert" accesskey="n"><span class="glyphicon glyphicon-plus"></span> Add</a>
				<a class="btn btn-info" role="button" tabindex="-1" href="monthly_sum.php?cmd=full"  accesskey="f"><span class="glyphicon glyphicon-zoom-in"></span> Full View</a>
				<a class="btn btn-info" role="button" tabindex="-1" href="monthly_sum.php?cmd=import"  accesskey="i"><span class="glyphicon glyphicon-open-file"></span> Import</a>
				<a class="btn btn-info" role="button" tabindex="-1" href="monthly_sum.php?cmd=export"  accesskey="e"><span class="glyphicon glyphicon-save-file"></span> Export</a>
				<a class="btn btn-info" role="button" tabindex="-1" href="monthly_sum.php?cmd=report"><span class="glyphicon glyphicon-print"></span> Report</a>

			  </div>
			  <br>
			  <div id="monthly_sum_search_collapse" class="collapse">
			  		

				  	 <form class="form-horizontal" action="" method="get">
					  <input type="hidden" name="cmd" value="search"><div class="form-group">
							    <label class="control-label col-sm-2"  for="month">Month:</label>
							    <div class="col-sm-4">
							    <input class="form-control" name="filter[month]" id="month">
							    </div>
							  </div><div class="form-group">
							    <label class="control-label col-sm-2"  for="project">Project:</label>
							    <div class="col-sm-4">
							    <input class="form-control" name="filter[project]" id="project">
							    </div>
							  </div><div class="form-group">
							    <label class="control-label col-sm-2"  for="model">Model:</label>
							    <div class="col-sm-4">
							    <input class="form-control" name="filter[model]" id="model">
							    </div>
							  </div><div class="form-group">
							    <label class="control-label col-sm-2"  for="mtarget">Mtarget:</label>
							    <div class="col-sm-4">
							    <input class="form-control" name="[mtarget]" id="mtarget">
							    </div>
							  </div><div class="form-group">
							    <label class="control-label col-sm-2"  for="ctarget">Ctarget:</label>
							    <div class="col-sm-4">
							    <input class="form-control" name="[ctarget]" id="ctarget">
							    </div>
							  </div><div class="form-group">
							    <label class="control-label col-sm-2"  for="ca">Ca:</label>
							    <div class="col-sm-4">
							    <input class="form-control" name="[ca]" id="ca">
							    </div>
							  </div><div class="form-group">
							    <label class="control-label col-sm-2"  for="percentage">Percentage:</label>
							    <div class="col-sm-4">
							    <input class="form-control" name="[percentage]" id="percentage">
							    </div>
							  </div><div class="form-group">
							    <label class="control-label col-sm-2"  for="delta">Delta:</label>
							    <div class="col-sm-4">
							    <input class="form-control" name="[delta]" id="delta">
							    </div>
							  </div>
					  <div class="form-group">        
					      <div class="col-sm-offset-2 col-sm-10">
					        <button type="submit" class="btn btn-default">Submit</button>
					      </div>
					    </div>
					</form>

			  </div>
			  	
			  

			  <table class="table table-striped table-hover">
			    <thead>
			      <tr>
			        <?php $columntitle[0]["value"]="Month";
$columntitle[0]["column"]="month";
$columntitle[1]["value"]="Project";
$columntitle[1]["column"]="project";
$columntitle[2]["value"]="Model";
$columntitle[2]["column"]="model";
$columntitle[3]["value"]="Mtarget";
$columntitle[3]["column"]="mtarget";
$columntitle[4]["value"]="Ctarget";
$columntitle[4]["column"]="ctarget";
$columntitle[5]["value"]="Ca";
$columntitle[5]["column"]="ca";
$columntitle[6]["value"]="Percentage";
$columntitle[6]["column"]="percentage";
$columntitle[7]["value"]="Delta";
$columntitle[7]["column"]="delta";

			        	$order_html=$order=="asc"?' <span class="glyphicon glyphicon-triangle-top"></span>':' <span class="glyphicon glyphicon-triangle-bottom"></span>';
			        	$order_link_html=$order=="asc"?'&order=desc':'&order=asc';
			        	foreach($columntitle as $title)
			        	{
			        		
			        		if(strcmp($sort_by,$title["column"])==0)
		        				echo "<th class=\"clickable-header\" data-href=\"monthly_sum.php?sort=".$title["column"].$order_link_html."\">".$title["value"].$order_html."</th>";
		        			else
			        			echo "<th class=\"clickable-header\"  data-href=\"monthly_sum.php?sort=".$title["column"]."\">".$title["value"]."</th>";
			        	}
			        	?>
			        	
			       </tr>
			    </thead>
			    <tbody>
			     <?php echo $list_html;?>
			    </tbody>
			  </table>
			  <?php echo $pagination_html;?>
			</div>
			<?php
		}
		else{
			$sql_count="select *  from monthly_sum where monthly_sum.is_deleted=0  ";
			$limit=10;
			$page=isset($_GET['page'])?$_GET['page']:1;
			$pagination_html="";
			$limit_sql="";
			if ($result_count=$mysqli->query($sql_count))
			  {
				  if($result_count->num_rows>$limit )
				  {
					  	$limit_sql=" limit ".($page-1)*$limit.",".$limit;
					  	$total_pages = ceil($result_count->num_rows / $limit);
					  	$pagination_html='<div class="container"  style="float:right;width:auto;"><ul class="pagination">';
					  	if($page>1)
					  	{
					  		$pagination_html.='<li class="page-item"><a class="page-link" href="monthly_sum.php?page=1" aria-label="Previous"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>';	
					  	}
					  	
					  	for($i=(($page-1)>0?($page-1):1);$i<=(($page+1)<$total_pages?$page+1:$total_pages);$i++)
					  	{
					  		if($page==$i)
					  			$pagination_html.='<li class="page-item active"><span class="page-link">'.$page.' <span class="sr-only">(current)</span></span></li>';
					  		else
					  			$pagination_html.='<li><a href="monthly_sum.php?page='.$i.'">'.$i.'</a></li>';
					  	}
					  		

					  	if($page<$total_pages)
					  	{
					  		$pagination_html.='<li class="page-item"><a class="page-link" href="monthly_sum.php?page='.($total_pages).'" aria-label="Next"><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>';	
					  	}
					  	
					  	
					  	$pagination_html.='</ul></div>';
				  }
			  }

		
			$sort_by=isset($_GET['sort'])?$_GET['sort']:"id";
			$order=isset($_GET['order'])?$_GET['order']:"asc";
			$sql="select monthly_sum.* from monthly_sum where monthly_sum.is_deleted=0  order by monthly_sum.".$sort_by." ".$order.$limit_sql;
			$list_html="";
			if ($result=$mysqli->query($sql))
			  {
			  if($result->num_rows>0)
			  {
			    while($row=$result->fetch_array(MYSQLI_ASSOC))
			    {
				   	 $list_html.="<tr>";
				     $list_html.= "<td>".$row["month"]."</td><td>".$row["project"]."</td><td>".$row["model"]."</td><td>".$row["mtarget"]."</td><td>".$row["ctarget"]."</td><td>".$row["ca"]."</td><td>".$row["percentage"]."</td><td>".$row["delta"]."</td>";
				     $list_html.= "<td><a href=\"monthly_sum.php?cmd=view&id=".$row["id"]."\"><span aria-hidden=\"true\" class=\"glyphicon glyphicon-eye-open\"></span></a></td>";
				     $list_html.= "<td><a href=\"monthly_sum.php?cmd=edit&id=".$row["id"]."\"><span aria-hidden=\"true\" class=\"glyphicon glyphicon-edit\"></span></a></td>";
				     $list_html.= "<td><a href=\"monthly_sum.php?cmd=delete&id=".$row["id"]."\"><span aria-hidden=\"true\" class=\"glyphicon glyphicon-trash\"></span></a></td>";
				     $list_html.="</tr>";
			    }
			  }
			  }
			?>
			<div class="container">
			  <h2 data-toggle="collapse" data-target="#monthly_sum_main_collapse">Monthly Sum</h2>
			  <div id="monthly_sum_main_collapse" class="collapse">
			  	<button class="btn btn-info" role="button"  tabindex="-1"  data-toggle="collapse" data-target="#monthly_sum_search_collapse" ><span class="glyphicon glyphicon-search"></span> Search</button>
			  	<a class="btn btn-info" role="button"  tabindex="-1" href="monthly_sum.php?cmd=insert" accesskey="n"><span class="glyphicon glyphicon-plus"></span> Add</a>
				<a class="btn btn-info" role="button" tabindex="-1" href="monthly_sum.php?cmd=full"  accesskey="f"><span class="glyphicon glyphicon-zoom-in"></span> Full View</a>
				<a class="btn btn-info" role="button" tabindex="-1" href="monthly_sum.php?cmd=import"  accesskey="i"><span class="glyphicon glyphicon-open-file"></span> Import</a>
				<a class="btn btn-info" role="button" tabindex="-1" href="monthly_sum.php?cmd=export"  accesskey="e"><span class="glyphicon glyphicon-save-file"></span> Export</a>
				<a class="btn btn-info" role="button" tabindex="-1" href="monthly_sum.php?cmd=report"><span class="glyphicon glyphicon-print"></span> Report</a>

			  </div>
			  <br>
			  <div id="monthly_sum_search_collapse" class="collapse">
			  		

				  	 <form class="form-horizontal" action="" method="get">
					  <input type="hidden" name="cmd" value="search"><div class="form-group">
							    <label class="control-label col-sm-2"  for="month">Month:</label>
							    <div class="col-sm-4">
							    <input class="form-control" name="filter[month]" id="month">
							    </div>
							  </div><div class="form-group">
							    <label class="control-label col-sm-2"  for="project">Project:</label>
							    <div class="col-sm-4">
							    <input class="form-control" name="filter[project]" id="project">
							    </div>
							  </div><div class="form-group">
							    <label class="control-label col-sm-2"  for="model">Model:</label>
							    <div class="col-sm-4">
							    <input class="form-control" name="filter[model]" id="model">
							    </div>
							  </div><div class="form-group">
							    <label class="control-label col-sm-2"  for="mtarget">Mtarget:</label>
							    <div class="col-sm-4">
							    <input class="form-control" name="[mtarget]" id="mtarget">
							    </div>
							  </div><div class="form-group">
							    <label class="control-label col-sm-2"  for="ctarget">Ctarget:</label>
							    <div class="col-sm-4">
							    <input class="form-control" name="[ctarget]" id="ctarget">
							    </div>
							  </div><div class="form-group">
							    <label class="control-label col-sm-2"  for="ca">Ca:</label>
							    <div class="col-sm-4">
							    <input class="form-control" name="[ca]" id="ca">
							    </div>
							  </div><div class="form-group">
							    <label class="control-label col-sm-2"  for="percentage">Percentage:</label>
							    <div class="col-sm-4">
							    <input class="form-control" name="[percentage]" id="percentage">
							    </div>
							  </div><div class="form-group">
							    <label class="control-label col-sm-2"  for="delta">Delta:</label>
							    <div class="col-sm-4">
							    <input class="form-control" name="[delta]" id="delta">
							    </div>
							  </div>
					  <div class="form-group">        
					      <div class="col-sm-offset-2 col-sm-10">
					        <button type="submit" class="btn btn-default">Submit</button>
					      </div>
					    </div>
					</form>

			  </div>
			  	
			  

			  <table class="table table-striped table-hover">
			    <thead>
			      <tr>
			        <?php $columntitle[0]["value"]="Month";
$columntitle[0]["column"]="month";
$columntitle[1]["value"]="Project";
$columntitle[1]["column"]="project";
$columntitle[2]["value"]="Model";
$columntitle[2]["column"]="model";
$columntitle[3]["value"]="Mtarget";
$columntitle[3]["column"]="mtarget";
$columntitle[4]["value"]="Ctarget";
$columntitle[4]["column"]="ctarget";
$columntitle[5]["value"]="Ca";
$columntitle[5]["column"]="ca";
$columntitle[6]["value"]="Percentage";
$columntitle[6]["column"]="percentage";
$columntitle[7]["value"]="Delta";
$columntitle[7]["column"]="delta";

			        	$order_html=$order=="asc"?' <span class="glyphicon glyphicon-triangle-top"></span>':' <span class="glyphicon glyphicon-triangle-bottom"></span>';
			        	$order_link_html=$order=="asc"?'&order=desc':'&order=asc';
			        	foreach($columntitle as $title)
			        	{
			        		
			        		if(strcmp($sort_by,$title["column"])==0)
		        				echo "<th class=\"clickable-header\" data-href=\"monthly_sum.php?sort=".$title["column"].$order_link_html."\">".$title["value"].$order_html."</th>";
		        			else
			        			echo "<th class=\"clickable-header\"  data-href=\"monthly_sum.php?sort=".$title["column"]."\">".$title["value"]."</th>";
			        	}
			        	?>
			        	
			       </tr>
			    </thead>
			    <tbody>
			     <?php echo $list_html;?>
			    </tbody>
			  </table>
			  <?php echo $pagination_html;?>
			</div>
			<?php
		}
		?>
		<br>
		
		</div>
		</body>
		<script type="text/javascript">
						jQuery(document).ready(function($) {
						    $(".clickable-header").click(function() {
						        window.document.location = $(this).data("href");
						    });
						});
						$(function() {

						    var start = moment().subtract(29, "days");
						    var end = moment();

						    function cb(start, end) {
						        
						    }

						    

						    cb(start, end);
						    
						});
			       </script>
		</html> 