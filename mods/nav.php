<?php 
	$query="select * from builder_modules order by added desc";
	$result=$mysqli->query($query);
	$html='<nav class = "navbar navbar-default" role = "navigation">
   <style>
   .navbar {
    margin-bottom: 0;
    }
   </style>
   <div class = "navbar-header">
      <button type = "button" class = "navbar-toggle" 
         data-toggle = "collapse" data-target = "#example-navbar-collapse">
         <span class = "sr-only">Toggle navigation</span>
         <span class = "icon-bar"></span>
         <span class = "icon-bar"></span>
         <span class = "icon-bar"></span>
      </button>
    
      <a class = "navbar-brand" href = "#">Rising Star - Production[Demo]</a>
   </div>
   
   <div class = "collapse navbar-collapse" id = "example-navbar-collapse">
  
      <ul class = "nav navbar-nav">';
	while($row=$result->fetch_array(MYSQLI_ASSOC))
	{
		if(strcasecmp($row['table_name'].'.php', basename($_SERVER['SCRIPT_NAME']))==0)
			$html.='<li class="active"><a href="'.$row['table_name'].'.php">'.$row['form_name'].' <span class="sr-only">(current)</span></a></li>';
		else
			$html.='<li><a href="'.$row['table_name'].'.php">'.$row['form_name'].'</a></li>';
	}
	$html.='</ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Hi User! <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Profile</a></li>';
            if(isset($_SESSION['is_admin']))
            {
              $html.='<li><a href="users.php?cmd=manage_users">Manage Users</a></li>';
            }
            $html.='<li role="separator" class="divider"></li>
            <li><a href="index.php?logout">Logout</a></li>
          </ul>
        </li>
      </ul>
   </div>
</nav>';
	echo $html;
	unset($html);

?>

