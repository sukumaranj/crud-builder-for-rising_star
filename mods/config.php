<?php
error_reporting(E_ALL);
session_start();
define("DATABASENAME","rising_star");
$mysqli=mysqli_connect("localhost","root","",DATABASENAME);

function is_logged_In()
{
	if(isset($_SESSION['user_id']) and $_SESSION['user_id']>0)
	{
		return true;
	}else{
		header('Location:../index.php'); 
	}
}

function validateDate($date, $format = 'Y-m-d H:i:s')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

?>
