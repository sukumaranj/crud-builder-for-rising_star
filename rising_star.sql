-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 16, 2017 at 11:52 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rising_star`
--

-- --------------------------------------------------------

--
-- Table structure for table `builder_modules`
--

CREATE TABLE IF NOT EXISTS `builder_modules` (
  `id` int(11) NOT NULL,
  `table_name` varchar(100) NOT NULL,
  `added` datetime DEFAULT CURRENT_TIMESTAMP,
  `form_name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `builder_modules`
--

INSERT INTO `builder_modules` (`id`, `table_name`, `added`, `form_name`) VALUES
(1, 'monthly_sum', '2017-02-08 09:23:28', 'Monthly Sum');

-- --------------------------------------------------------

--
-- Table structure for table `builder_reports`
--

CREATE TABLE IF NOT EXISTS `builder_reports` (
  `id` int(11) NOT NULL,
  `query` text NOT NULL,
  `created_date` datetime NOT NULL,
  `name` varchar(200) NOT NULL,
  `module_id` int(11) NOT NULL,
  `script` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `builder_reports`
--

INSERT INTO `builder_reports` (`id`, `query`, `created_date`, `name`, `module_id`, `script`) VALUES
(1, 'SELECT model as x,mtarget as y FROM `monthly_sum` where month=''november''', '2016-11-08 00:00:00', 'Model Mtarget', 1, 'keys: { \n					            	x:"x",\n					            	value: ["y"] \n					            	},\n              				names:{\n              					y:"Mtarget"\n              					},\n              				type:"bar"\n            				},\n            				axis: { \n            					x: { \n            						type : "categorized", \n            						},\n            					}'),
(2, 'SELECT model as x,mtarget as y1,ctarget as y2 FROM `monthly_sum` where month=''november''', '2016-11-08 00:00:00', 'Model', 1, 'keys: { \n										            	x:"x",\n										            	value: ["y1","y2"] \n										            	},\n					              				names:{\n					              					y1:"Mtarget",\n					              					y2:"Ctarget",\n					              					},\n					              				type:"bar"\n					            				},\n					            				axis: { \n					            					x: { \n					            						type : "categorized", \n					            						},\n					            					}'),
(8, 'SELECT model as x,mtarget as y1,ctarget as y2 FROM `monthly_sum` where month=''november''', '2016-11-08 00:00:00', 'Model comparision', 1, 'keys: { \r\n										            	x:"x",\r\n										            	value: ["y1","y2"] \r\n										            	},\r\n					              				names:{\r\n					              					y1:"Mtarget",\r\n					              					y2:"Ctarget",\r\n					              					},\r\n					              				type:"spline"\r\n					            				},\r\n					            				axis: { \r\n					            					x: { \r\n					            						type : "categorized", \r\n					            						},\r\n					            					}'),
(9, 'select (g/f)*100 as closed from( select *,(SELECT sum(ctarget) FROM `monthly_sum` where month=''november'') as g from (SELECT sum(mtarget) as f FROM `monthly_sum` where month=''november'') f) o', '2016-11-08 00:00:00', 'target Gauge', 1, 'keys: { value: ["closed"] },\ntype:"gauge",\n           },\n           gauge: {\n                   title: "Reporting Mode"   }'),
(10, 'SELECT date(month) as x,\n(select sum(ctarget) from monthly_sum where project=''P1'') as P1,\n(select sum(ctarget) from monthly_sum where project=''P2'') as P2,\n(select sum(ctarget) from monthly_sum where project=''P3'') as P3,\n(select sum(ctarget) from monthly_sum where project=''P4'') as P4\nFROM `monthly_sum` c group by date(project)', '2016-11-08 00:00:00', 'Pie Delta', 1, 'keys: { \nvalue: ["P1","P2","P3","P4"] },\nnames:{P1:"P1",P2:"P2",P3:"P3",P4:"P4"},\ntype:"pie",\n},');

-- --------------------------------------------------------

--
-- Table structure for table `builder_uploads`
--

CREATE TABLE IF NOT EXISTS `builder_uploads` (
  `id` int(11) NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `upload_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `columns` text NOT NULL,
  `details` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `builder_users`
--

CREATE TABLE IF NOT EXISTS `builder_users` (
  `id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(256) NOT NULL,
  `name` varchar(200) NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `builder_users`
--

INSERT INTO `builder_users` (`id`, `username`, `password`, `email`, `name`, `is_admin`) VALUES
(1, 'admin', 'admin', 'admin@herakle.com', 'admin', 1),
(2, 'user2', 'user2', 'user2@herakle.com', 'user2', 0),
(3, 'user3', 'user3', 'user3@herakle.com', 'user3', 0);

-- --------------------------------------------------------

--
-- Table structure for table `monthly_sum`
--

CREATE TABLE IF NOT EXISTS `monthly_sum` (
  `id` int(11) NOT NULL,
  `month` varchar(30) NOT NULL,
  `project` varchar(30) NOT NULL,
  `model` varchar(30) NOT NULL,
  `mtarget` int(11) NOT NULL,
  `ctarget` int(11) NOT NULL,
  `ca` int(11) NOT NULL,
  `percentage` int(11) NOT NULL,
  `delta` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_modified_on` datetime NOT NULL,
  `last_modified_by` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_on` datetime NOT NULL,
  `deleted_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `monthly_sum`
--

INSERT INTO `monthly_sum` (`id`, `month`, `project`, `model`, `mtarget`, `ctarget`, `ca`, `percentage`, `delta`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `is_deleted`, `deleted_on`, `deleted_by`) VALUES
(1, 'NOVEMBER', 'P1', 'P1.2', 4600, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 0),
(2, 'NOVEMBER', 'P1', 'P1.3', 23000, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 0),
(3, 'NOVEMBER', 'P1', 'P1.4', 95000, 16100, 16422, 17, -78578, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 0),
(4, 'NOVEMBER', 'P1', 'P1.5', 3100, 510, 340, 11, -2760, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 0),
(5, 'NOVEMBER', 'P2', 'P2.1', 28000, 20000, 20385, 73, -7615, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 0),
(6, 'NOVEMBER', 'P2', 'P2.2', 279000, 232000, 230447, 83, -48553, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 0),
(7, 'NOVEMBER', 'P3', 'P3.1', 2000, 25000, 0, 0, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 0),
(8, 'NOVEMBER', 'P3', 'P3.2', 1000, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 0),
(9, 'NOVEMBER', 'P4', 'P4.1', 55000, 52800, 47470, 86, -7530, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 0),
(10, 'NOVEMBER', 'P4', 'P4.2', 295000, 28700, 23438, 8, -271562, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 0),
(11, 'NOVEMBER', 'P4', 'P4.3', 104000, 62000, 45116, 43, -58884, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 0),
(12, 'NOVEMBER', 'P4', 'P4.4', 450000, 113947, 105724, 23, -344276, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 0),
(13, 'NOVEMBER', 'P4', 'P4.5', 24000, 1393, 2182, 9, -21818, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 0),
(14, 'NOVEMBER', 'P4', 'P4.6', 416000, 126861, 103602, 25, -312398, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `builder_modules`
--
ALTER TABLE `builder_modules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `table_name` (`table_name`);

--
-- Indexes for table `builder_reports`
--
ALTER TABLE `builder_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `builder_uploads`
--
ALTER TABLE `builder_uploads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `builder_users`
--
ALTER TABLE `builder_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `monthly_sum`
--
ALTER TABLE `monthly_sum`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `builder_modules`
--
ALTER TABLE `builder_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `builder_reports`
--
ALTER TABLE `builder_reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `builder_uploads`
--
ALTER TABLE `builder_uploads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `builder_users`
--
ALTER TABLE `builder_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `monthly_sum`
--
ALTER TABLE `monthly_sum`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
